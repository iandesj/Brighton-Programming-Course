# second response program (one of many possible solutions)


# function definition
def get_level(years)
	case years
	when 0
		puts "Total newbie"
	when 1..2
		puts "Still a newbie"
	when 3..4
		puts "You've got some experience"
	when 5..10
		puts "You've got the hang of things"
	when 10..100
		puts "Master programmer!"
	else
		puts "You're the best!!"
	end
end

# asking for input
puts "How many years have you been programming?"
years = gets.chomp.to_i

# calling function and passing in 'years' to get_level function
get_level(years)



