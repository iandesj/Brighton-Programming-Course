# Brighton-Programming-Course
My course materials for the Brighton Community Education Programming Course I'm teaching. Keepin' it simple and n00b friendly for the kid-o's.

(curriculum still in the works)


Week 1
======
##Class 1
###Overview of Programming Class
- Explanation of what programming "is".
- Different styles of programming and it's uses.
- What I want you (the students) to learn.
- Explain how I'm going to teach, and how each class is going to be layed out.
- Not necessarily teaching you how to write best practice or high performance code, rather basic programming concepts that will help in later classes or applications.
- LEARN TO THINK LIKE A PROGRAMMER. LOGIC IS IMPORTANT. THE COMPUTER WILL DO WHAT YOU TELL IT TO.
- How to setup your computer to start programming (in Ruby)!

##Class 2
###Teach:
- Basic output of strings and other variables.
- Using comments in code.
- Basic use of variables and variable naming.
  - Declaring variables as strings, integers, decimals, etc.
  - Using variables to do math.
  - Using variables and printing them.
- Printing variables embedded in strings.
- Prompting for input to the command-line.

###Response:
- Write code that prints your name, birthday, and favorite band. Make sure each string prints on a different line.

- Write code that prints the same information, except put these strings in variables. Add comments to describe what your different lines of code does.

- Write a small program that asks for input of at least two numbers. Your code should add these numbers up and display them to the screen. Add comments to describe what your code is doing.

Week 2
======
##Class 3
###Teach:
- Boolean logic & variables.
- Begin the basics of Boolean logic.
  - If/Else statements.
  - Case expressions.
- Basic function writing and usage.
  - Functions with and without parameters.
  - Functions that return a value.
- Putting it all together.
  - Functions with logic inside them.

###Response:
- Create a program that takes user input (from week 1 lesson) and prints out a different value depending on what is input. User your imagination, and use either if/else/elsif statements or case expressions. Comments are appreciated.
- Write code that does what the first program does, but put that code in a function and call it. Your called function should take that user input, which gets passed INTO the function, and evaluated within that functions logic, then printed out to the screen as the first program did. Hint: Use code from your previous program and simply wrap it a function.

##Class 4
###Teach:
- More functions.
- Simple modules with functions.
- Putting it all together, thus far.
   - Passing input into variables, passing the variables into functions, returning values from functions.
   - Writing multiple modules, using different functions from different modules.

###Response:
- Write a program containing three functions. 
  - The first function takes in two numbers, divides those two numbers properly, and returns the value (hint: you cannot divide by zero, add logic so that this function accommodates this condition.) 
  - The second function takes in two strings, let the function concatenate these two strings and return it to the user. If you feel ambitious, add logic that detects the type of function parameter and only return the concatenated string if both values are strings (feel free to Google your heart out.) 
  - The last function can do whatever you want, be sure to add comments describing it's functionality (no pun intended).
- Put each one of these functions in it's own module, call these modules ModOne, ModTwo, and MyMod. Feel free to add more functions to these modules, exploring more about what you've learned.

Week 3
======
##Class 5
###Teach:
- Basic concept of loops.
- Common loops include While, For, Each, Until.
  - Operators like Break, Next, Redo, Retry
- Touch on Exceptions.
  - When to use Exceptions?

###Response:
- Write 3 programs that print out numbers (1-10). Each program should use a different style of loop to accomplish this simple task.
- Build a program with a function that takes in an integer number. This number should be used in a loop and once the count of the loop is equal to the function parameter number, the function should print out a string saying it has reached said number. User should have to enter an input, which gets passed to the function. Please comment your code.
- Incorporate the use of an exception in one or more of your loop programs.

##Class 6

Week 4 (final week)
===================
##Class 7
###Teach:
- Basic concepts of Object Oriented and Classes
  - Taking a group of functions and data and placing them inside a container so you can access them with the . (dot) operator.
  - Writing class functions, initializing variables, instance variables.
- Using classes within different parts of code and instantiating (creating) new class object instances.

###Response:
- Write a program with a single class, call the class Animal. The class should take in two strings, breed, name, and sound of animal (if any). This class should have an initializer function that puts these three strings into instance variables. The class should also have two other simple functions; the first function called ''sound'' that prints out the sound that animal makes (if any), the second function named ''display'' will print out something along the lines of "I am a <breed of animal> and my name is <name of animal>!".
- If you have time, make a similar program, except change the class to take in just two strings (breed, name) and have logic in the ''sound'' function that will output different sound depending on what the choice of the breed is. Hint: use if/else or the when operator.

##Class 8
###Teach:
- Quick overall review of the taught programming concepts.
- If time - talk about some cool characteristics of Ruby
  - Cool included libraries within the Ruby-core language
  - Ruby Gems
  - Web applications and Framework usage
  - Recommendations for Ruby tutorials, Rails, Sinatra, and other good resources for new programmers.
