# example code showcasing functions with logic embedded in them


# building on the similar functions we wrote in the last code

# simple function to return email of user, if available. Parameter is username

username1 = "iand"
username2 = "carljones"
username3 = "billgates"

def get_email(username)
	if username == "iand"
		return "iand@email.com"
	elsif username == "rickjackson"
		return "rick@email.com"
	elsif username == "carljones"
		return "carl@email.com"
	else
		return "No email on file."
	end
end

# calling function and printing out returned values
puts get_email(username1)
puts get_email(username2)
puts get_email(username3)


# same functionality, but using case statement

def get_email(username)
	case username
	when "iand"
		return "iand@email.com"
	when "rickjackson"
		return "rick@email.com"
	when "carljones"
		return "carl@email.com"
	else
		return "No email on file."
	end
end

# calling function and printing out returned values
puts get_email(username1)
puts get_email(username2)
puts get_email(username3)







