# example program with function usage


# simple functions with no parameters

def print_name
	puts "Ian DesJardins"
end

def get_dinner
	puts "What do you want for dinner?"
	dinner = gets.chomp
	puts "You are having #{dinner} for dinner today."
end

# calling (executing) these defined functions

print_name

get_dinner


# simple functions with parameters

first = "Ian"
last = "DesJardins"

# prints first name when passed
def print_name(first_name)
	puts first_name
end

# prints first and last name when passed
def print_full(first_name, last_name)
	puts first_name + " " + last_name
end

# calling these defined functions with parameters

print_name(first)

print_full(first, last)


# simple functions with parameters and return values

first = "Abraham"
last = "Lincoln"

def get_name(first_name)
	return first_name #return value
end

def get_full(first_name, last_name)
	return first_name + " " + last_name #return value
end

# calling these defined functions and obtaining the value returned

get_name(first) # calling function, returns value

puts get_name(first) # calling function, returns value, we're printing the returned value


puts get_full(first, last)

full_name = get_full(first, last) # can also write the returned value to a new variable and use it as you please
puts full_name



