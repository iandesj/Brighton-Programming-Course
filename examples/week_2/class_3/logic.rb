# example program of boolean logic

name = "Ian"
month = "February"
age = 14


# if/else statement examples

if name == "Ian"
	puts "My name is Ian."
else
	puts "Not sure what my name is."
end

if month == "February"
	puts "Birth month is February."
else
	puts "Not sure what the birth month is."
end

if age == 14
	puts "My age is #{age}."
else
	puts "Not sure what my age is."
end


# if/elsif/else statement examples

if name == "Roger"
	puts "My name is Roger."
elsif name == "Ian"
	puts "My name is Ian."
else
	puts "Not sure what my name is."
end

if month == "March"
	puts "Birth month is March."
elsif month == "February"
  puts "Birth month is February."
else
  puts "Not sure what the birth month is."
end

if age == 25
	puts "My age is #{age}."
elsif age == 14
  puts "My age is #{age}."
else
  puts "Not sure what my age is."
end


# case expressions examples

case month
when "January"
	puts "Birth month is January."
when "February"
	puts "Birth month is February."
when "March"
	puts "Birth month is March."
when "April"
	puts "Birth month is April."
when "May"
	puts "Birth month is May."
when "June"
	puts "Birth month is June."
when "July"
	puts "Birth month is July."
when "August"
	puts "Birth month is August."
when "September"
	puts "Birth month is September."
when "October"
	puts "Birth month is October."
when "November"
	puts "Birth month is November."
when "December"
	puts "Birth month is December."
else
	puts "Were you even born?"
end
