# example of boolean variables

# assigning the variable hungry to true
hungry = true

# assigning the variable cranky to true
cranky = true

# assigning the variable have_food to false
have_food = false

