Guide to setting up Ruby programming environment for Brighton Community Education Programming Course
====================================================================================================

We are setting up our environment for potential use on ANY operating system platform. To do this, we are going to create an account to a service that runs in the cloud, it is like a personal computer that we can access from anywhere to accomplish our programming tasks.

Follow these steps to get started...

1. Open up your web browser, preferrebly Firefox or Google Chrome. I''m sure this would work on IE, but I''ve never tried this yet.
2. Log on to www.nitrous.io
3. We must sign up (unless already done) by creating a username, entering our email and a new password. The second alternative is logging into this via our Google or LinkedIn account if that is an easier option.
4. After account creation and login, we must click the ''Open Dashboard'' button to get started with our development environment.
5. This page is where we manage our ''Boxes'' or our virtual computers we will use for programming. Proceed by clicking the ''New Box'' button.
6. A list of different templates to load into our ''Box'' will show up, we will just stick with the Ruby/Rails selection. Give it a name of ''programming-class-1-(your initials)'' or name it whatever you''d like! Leave all other options as-is and click ''Create Box''.
7. At this point, there will be a meter that shows the status of the ''Boxes'' creation.
8. Once your box has been created, it will show the text editor and the tools we will use for our programming environment. On the left hand panel, this is a list of files and folders that we will find on our box. On the right side, this is our text editor, or the panel we will use to type our programming code in. The bottom panel, is our terminal/command-line/console, and is essentially the interface we will use to execute and run our programs, as well as where the output or text will be printed when we run said programs.
9. Last but not least, right click the ''workspace'' folder in the left panel, and click ''Set workspace as Root''.

YOU''VE FINISHED! Now you can get started and have a full featured Ruby programming environment.


