# example code to show printing strings and variables

# printing string and/or number without variable
puts "Hello, World!"
puts "Hi, my name is Crazy."
puts 100
puts 100.98
puts 10 + 11 + 0.5


# printing variables with assigned values

first_name = "Ian"
last_name = "DesJardins"
age = 14

puts first_name
puts last_name
puts age


# printing variables embedded into strings
# This notation ->  #{variable_name} when used a string can be used to print out the contents of a variable within a string

puts "My name is #{first_name}."
puts "My last name is #{last_name}."
puts "My full name is #{first_name} #{last_name}."
puts "My name is #{first_name} and I am #{age} years of age."


