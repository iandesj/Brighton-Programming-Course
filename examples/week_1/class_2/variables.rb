# Example program showcasing strings and variables

# example of a variable initialized as a string
variable1 = "Hello!"

variable2 = "World!"

# example of a variable initialized as a number, more importantly an integer
variable3 = 10

# example of a variable initialized as a number, more specifically a "Floating point decimal"
variable4 = 10.5

variable5 = 5.6789

# You can name a variable whatever you'd like, most programming languages are case sensitive so you could have
number = 10
# and
NUMBER = 10
# in the same code



