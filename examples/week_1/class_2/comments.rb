# My first computer program, in Ruby!

# THIS IS A COMMENT
# Coments don't print out with the program, they are merely for the programmers convenience

puts "Hello, World!" # this code prints out exactly what it says!

=begin
	this is how
	to write comments
	on multiple
	lines
=end

puts "More code under the multiple line comment"


