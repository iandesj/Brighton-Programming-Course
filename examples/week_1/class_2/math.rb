# Example program showcasing variables being used for simple math functions

# example of a variable initialized as a number, more importantly an integer
num1 = 10

# example of a variable initialized as a number, more specifically a "Floating point decimal"
num2 = 10.5

num3 = 5.6789

# using two numbers in an addition problem and writing the sum to a new variable called number_sum
number_sum = num1 + num2 # this would evaluate to 20.5

number_sum2 = num2 + num3 # this would evaluate to 16.1789



